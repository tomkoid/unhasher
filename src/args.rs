use clap::Parser;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
pub struct Args {
    #[arg(short = 't', long)]
    pub hash: String,

    #[arg(short, long)]
    pub wordlist: String,
}

impl Args {
    pub fn new() -> Self {
        Args::parse()
    }
}
