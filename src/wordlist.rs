use std::io::BufRead;

#[derive(Debug)]
pub struct Wordlist {
    pub content: Vec<String>,
    pub skipped_lines: u64,
}

impl Wordlist {
    pub fn new(file_path: String) -> Result<Self, anyhow::Error> {
        let file = std::fs::File::open(&file_path)?;

        let io_buffer = std::io::BufReader::new(file);

        let mut lines = Vec::new();

        let mut skipped_lines = 0;

        for line in io_buffer.lines() {
            let line = match line {
                Ok(line) => line,
                Err(_) => {
                    skipped_lines += 1;
                    continue;
                }
            };

            lines.push(line);
        }

        Ok(Wordlist {
            content: lines,
            skipped_lines,
        })
    }
}
