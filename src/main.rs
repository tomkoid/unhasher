use sha2::{Digest, Sha256};
use spinners::{Spinner, Spinners};

pub mod args;
mod hash;
pub mod wordlist;

use crate::args::Args;
use crate::hash::Hash;
use crate::wordlist::Wordlist;

const SPINNER: Spinners = Spinners::Dots9;

fn main() -> Result<(), anyhow::Error> {
    let args = Args::new();

    let mut sp = Spinner::new(SPINNER, "wordlist: reading wordlist..".into());

    let wordlist = Wordlist::new(args.wordlist);

    let wordlist = if let Err(e) = &wordlist {
        sp.stop_with_message(format!("ERROR: {}", e));
        std::process::exit(1);
    } else {
        wordlist?
    };

    sp.stop_with_message("wordlist: finished reading wordlist".into());
    println!("wordlist: skipped lines: {}", wordlist.skipped_lines);

    let mut sp = Spinner::new(SPINNER, "hash: guessing the hash..".into());

    let result = Hash::new(args.hash, wordlist);

    let result = if let Err(e) = result {
        sp.stop_with_message(format!("ERROR: {}", e));
        std::process::exit(1);
    } else {
        result.unwrap()
    };

    sp.stop_with_message(format!("RESULT: {}", result.result));

    Ok(())
}
